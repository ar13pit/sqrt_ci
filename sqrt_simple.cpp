#include "gtest/gtest.h"
#include <iostream>

float squareRoot(float num) {

    if (num < 0) return -1;

    float temp, sqrt;
    float number;
    number = num;
    sqrt = number / 2;
    temp = 0;

    // It will stop only when temp is the square of our number
    while(sqrt != temp){

        // setting sqrt as temp to save the value for modifications
        temp = sqrt;

        // main logic for square root of any number (Non Negative)
        sqrt = ( number/temp + temp) / 2;
    }

    std::cout << "Square root of '" << number << "' is '" << sqrt << "'" << std::endl;

   return (sqrt);
}

TEST (SquareRootTest, PositiveNos) { 
    EXPECT_EQ (5.0, squareRoot (25.0));
    EXPECT_EQ (3.0, squareRoot (9.0));
    EXPECT_FLOAT_EQ (25.4, squareRoot (645.16));
}

TEST (SquareRootTest, ZeroAndNegativeNos) { 
    ASSERT_EQ (0.0, squareRoot (0.0));
    ASSERT_EQ (-1, squareRoot (-22.0));
}
